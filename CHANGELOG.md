# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2020-05-08
### Added
- Add Messages and contact form with google maps

## [1.0.0] - 2020-05-07
### Added
- First version of the website
