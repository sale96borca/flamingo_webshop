using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Flamingo.Data;
using Flamingo.Models;
using Flamingo.Models.Categories;
using Flamingo.Services.Interfaces;

namespace Flamingo.Services
{
    public class CategoryService : IServiceCollector<Category>
    {
        private readonly FlamingoDbContext context;
        public CategoryService(FlamingoDbContext _context)
        {
            context = _context;
        }

        public IEnumerable<Category> GetCollection()
        {
            throw new System.NotImplementedException();
        }

        public Task<List<Category>> GetCollectionById(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> RemoveCollection(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}