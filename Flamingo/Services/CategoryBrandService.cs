using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;
using Flamingo.Models;
using Flamingo.Data;
using Flamingo.Services.Interfaces;
using Flamingo.Models.Categories;
using Flamingo.Models.Brands;

namespace Flamingo.Services
{
    public class CategoryBrandService : IServiceCollector<CategoryBrandDisplay>
    {
        private readonly FlamingoDbContext context;
        public CategoryBrandService(FlamingoDbContext _context)
        {
            context = _context;
        }
        public IEnumerable<CategoryBrandDisplay> GetCollection()
        {
            var brands = context.Brands
                .Join(
                    context.CategoryBrands,
                    brand => brand.Id,
                    categoryBrand => categoryBrand.BrandId,
                    (brands, categoryBrand) => new CategoryBrandDisplay {
                        BrandId = brands.Id,
                        BrandName = brands.Name,
                        Url = brands.Url,
                        CategoryNames = context.Categories.Where(x => x.Id == categoryBrand.CategoryId).ToList()
                    }
                ).Distinct();

            return brands;
        }

        public async Task<bool> RemoveCollection(int id)
        {
            var collection = await context.CategoryBrands.Where(x => x.BrandId == id).ToListAsync();
            context.CategoryBrands.RemoveRange(collection);
            await context.SaveChangesAsync();

            return context.ChangeTracker.HasChanges();
        }

        public SelectList GetActiveSelectList(int id)
        {
            var brands = context.Brands
                .FirstOrDefault(x => x.Id == id);

            var categories = context.CategoryBrands
                .Where(x => x.BrandId == id)
                .OrderBy(x => x.CategoryId)
                .Join(
                    context.Categories,
                    brand => brand.CategoryId,
                    category => category.Id,
                    (brands, category) => new {
                        Id = brands.CategoryId,
                        Name = category.Name
                    }
                );
            
            var selectList = new SelectList(context.Categories
                .OrderBy(x => x.Name), "Id", "Name");

            foreach (var item in categories) {
                foreach(var select in selectList) {
                    if (select.Value == item.Id.ToString()) {
                        select.Selected = true;
                    }
                }
            }

            return selectList;
        }

        public Task<List<CategoryBrandDisplay>> GetCollectionById(int id)
        {
            throw new NotImplementedException();
        }
    }
}