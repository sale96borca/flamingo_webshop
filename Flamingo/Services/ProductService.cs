using System.Collections.Generic;
using System.Threading.Tasks;
using Flamingo.Data;
using Flamingo.Models;
using Flamingo.Models.Products;
using Flamingo.Services.Interfaces;

namespace Flamingo.Services
{
    public class ProductService : IServiceCollector<Product>
    {
        private readonly FlamingoDbContext context;
        public ProductService(FlamingoDbContext _context)
        {
            context = _context;
        }

        public IEnumerable<Product> GetCollection()
        {
            throw new System.NotImplementedException();
        }

        public Task<List<Product>> GetCollectionById(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> RemoveCollection(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}