using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Flamingo.Models;

namespace Flamingo.Services.Interfaces
{
    public interface IServiceCollector<T>
    {
        IEnumerable<T> GetCollection();
        Task<List<T>> GetCollectionById(int id);

        Task<bool> RemoveCollection(int id);
    }
}