using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flamingo.Data;
using Flamingo.Models;
using Flamingo.Models.Products;
using Flamingo.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Flamingo.Services
{
    public class ProductImageService : IServiceCollector<ProductImage>
    {
        private readonly FlamingoDbContext context;
        public ProductImageService(FlamingoDbContext _context)
        {
            context = _context;
        }
        public async Task<List<ProductImage>> GetCollectionById(int id)
        {
            return await context.ProductImages
                .Where(x => x.ProductId == id)
                .Include(x => x.Image)
                .ToListAsync();
        }

        public IEnumerable<ProductImage> GetCollection()
        {
            throw new NotImplementedException();
        }

        public Task<bool> RemoveCollection(int id)
        {
            throw new NotImplementedException();
        }
    }
}