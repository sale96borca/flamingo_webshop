using System.Linq;
using System.Threading.Tasks;
using Flamingo.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Flamingo.Models
{
    public class NavbarViewComponent : ViewComponent
    {
        private readonly FlamingoDbContext context;
        public NavbarViewComponent(FlamingoDbContext _context)
        {
            context = _context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var categories = await context.Categories
                .OrderBy(x => x.Name)
                .ToListAsync();

            return View(categories);
        }
    }
}