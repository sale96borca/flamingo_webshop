using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Flamingo.Models;
using Flamingo.Models.Categories;
using Flamingo.Models.Brands;

namespace Flamingo.Models.Products
{
    public class ProductFrontView
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required, Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]

        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
        [ForeignKey("BrandId")]
        public Brand Brand { get; set; }
        #nullable enable
        public Image? Tumbnail { get; set; }
    }
}