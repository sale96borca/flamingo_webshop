using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Flamingo.Models.Categories;
using Flamingo.Models.Brands;

namespace Flamingo.Models.Products
{
    public class Product
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required, Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public string Barcode { get; set; }
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        [Display(Name = "Brand")]
        public int BrandId { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
        [Display(Name="Featured")]
        public bool IsFeatured { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
        [ForeignKey("BrandId")]
        public virtual Brand Brand { get; set; }

        public Product()
        {
            CreatedAt = DateTime.Now;
            IsFeatured = false;
        }
    }
}