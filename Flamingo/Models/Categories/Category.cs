using System;
using System.ComponentModel.DataAnnotations;

namespace Flamingo.Models.Categories
{
    public class Category
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}