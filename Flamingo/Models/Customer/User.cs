using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Flamingo.Models.Customer
{
    public class User
    {
        [DisplayName("Username"), MinLength(4, ErrorMessage = "Username mora da ima bar 4 karaktera"), Required(ErrorMessage = "Morate da unesete Username")]
        public string UserName { get; set; }
        [DisplayName("Ime"), Required(ErrorMessage = "Morate da unesete Ime")]
        public string FirstName { get; set; }
        [DisplayName("Prezime"), Required(ErrorMessage = "Morate da unesete Prezime")]
        public string LastName { get; set; }
        [DataType(DataType.EmailAddress, ErrorMessage = "Email adresa nije validna."), EmailAddress, Required(ErrorMessage = "Morate da unesete Email")]
        public string Email { get; set; }
        [DisplayName("Broj telefona"), DataType(DataType.PhoneNumber, ErrorMessage = "Broj telefona nije validan.")]
        public string PhoneNumber { get; set; }
        [DisplayName("Ulica i broj")]
        public string StreetAddress { get; set; }
        [DisplayName("Grad")]
        public string City { get; set; }
        [DataType(DataType.PostalCode, ErrorMessage = "Zip nije validan."), DisplayName("Zip")]
        public string Zipcode { get; set; }
        [DisplayName("Lozinka"), Required(ErrorMessage = "Morate da unesete Lozinku")]
        public string Password { get; set; }
        public DateTime CreatedAt { get; set; }

        public User()
        {
        }

        public User(AppCustomer customer)
        {
            UserName = customer.UserName;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
            PhoneNumber = customer.PhoneNumber;
            StreetAddress = customer.StreetAddress;
            City = customer.City;
            Zipcode = customer.Zipcode;
            CreatedAt = customer.CreatedAt;
            Password = customer.PasswordHash;
        }
    }
}