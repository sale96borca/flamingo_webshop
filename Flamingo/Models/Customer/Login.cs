using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Flamingo.Models.Customer
{
    public class Login
    {
        [DataType(DataType.EmailAddress, ErrorMessage = "Email adresa nije validna."), EmailAddress, Required(ErrorMessage = "Morate da unesete Email")]
        public string Email { get; set; }
        
        [DisplayName("Lozinka"), Required(ErrorMessage = "Morate da unesete Lozinku")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}