using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Flamingo.Models.Customer
{
    public class RoleEdit
    {
        public IdentityRole Role { get; set; }
        public List<AppCustomer> Members { get; set; }
        public List<AppCustomer> NonMembers { get; set; }
        public string RoleName { get; set; }
        public string[] AddIds { get; set; }
        public string[] DeleteIds { get; set; }
    }
}