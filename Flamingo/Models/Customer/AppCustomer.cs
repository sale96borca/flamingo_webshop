using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Flamingo.Models.Customer
{
    public class AppCustomer : IdentityUser
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }

        public AppCustomer()
        {
            CreatedAt = DateTime.Now;
        }
    }
}