using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Flamingo.Models.Customer
{
    public class CustomerEdit
    {
        [DisplayName("Username"), MinLength(4, ErrorMessage = "Username mora da ima bar 4 karaktera")]
        public string UserName { get; set; }
        [DisplayName("Ime")]
        public string FirstName { get; set; }
        [DisplayName("Prezime")]
        public string LastName { get; set; }
        [DataType(DataType.EmailAddress, ErrorMessage = "Email adresa nije validna."), EmailAddress]
        public string Email { get; set; }
        [DisplayName("Broj telefona"), DataType(DataType.PhoneNumber, ErrorMessage = "Broj telefona nije validan.")]
        public string PhoneNumber { get; set; }
        [DisplayName("Ulica i broj")]
        public string StreetAddress { get; set; }
        [DisplayName("Grad")]
        public string City { get; set; }
        [DataType(DataType.PostalCode, ErrorMessage = "Zip nije validan."), DisplayName("Zip")]
        public string Zipcode { get; set; }
        public DateTime CreatedAt { get; set; }

        public CustomerEdit()
        {
        }

        public CustomerEdit(AppCustomer customer)
        {
            UserName = customer.UserName;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
            PhoneNumber = customer.PhoneNumber;
            StreetAddress = customer.StreetAddress;
            City = customer.City;
            Zipcode = customer.Zipcode;
            CreatedAt = customer.CreatedAt;
        }
    }
}