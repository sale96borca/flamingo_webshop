namespace Flamingo.Models.Checkout
{
    public class OrderStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}