using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Flamingo.Data;
using Flamingo.Models.Cart;
using Microsoft.AspNetCore.Http;

namespace Flamingo.Models.Checkout
{
    public class Order
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string StreetAddress { get; set; }
        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public int OrderStatusId { get; set; }
        
        [DataType(DataType.Currency), Column(TypeName = "decimal(18,2)"), Required]
        public decimal Price { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        public string Zipcode { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
        
        [ForeignKey("OrderStatusId")]
        public virtual OrderStatus OrderStatus { get; set; }

        [NotMapped]
        private List<CartItem> CartItems { get; set; }

        public Order()
        {
        }

        public Order(List<CartItem> cartItems = null)
        {
            DateCreated = DateTime.Now;
            CartItems = cartItems;
            Price = CartItems.Sum(x => x.Price * x.Quantity);
        }

        public List<OrderItem> SetItems(List<CartItem> cartItems = null)
        {
            CartItems ??= cartItems;
            
            List<OrderItem> orderItems = CartItems.Select(
                item => new OrderItem
                {
                    ProductId = item.ProductId, 
                    OrderId = Id, 
                    Quantity = item.Quantity
                }
            ).ToList();
            
            return orderItems;
        }
    }
}