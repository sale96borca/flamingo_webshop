using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Flamingo.Models.Checkout
{
    public class CheckoutView
    {
        [DisplayName("Ime")]
        [Required, RegularExpression(@"[A-z]{3,}", ErrorMessage = "Ime nije u validnom formatu")]
        public string FirstName { get; set; }
        [DisplayName("Prezime")]
        [Required, RegularExpression(@"[A-z]{3,}", ErrorMessage = "Prezime nije u validnom formatu")]
        public string LastName { get; set; }
        [DisplayName("Adresa"), Required]
        public string StreetAddress { get; set; }
        [DisplayName("Broj telefona")]
        [Required, DataType(DataType.PhoneNumber, ErrorMessage = "Broj telefona nije validan.")]
        public string PhoneNumber { get; set; }
        [DisplayName("Grad"), Required]
        public string City { get; set; }
        [Required, DataType(DataType.EmailAddress, ErrorMessage = "Email adresa nije validna.")]
        public string Email { get; set; }
        [DisplayName("Zip")]
        [DataType(DataType.PostalCode, ErrorMessage = "Zip nije validan")]
        public string Zipcode { get; set; }
    }
}