namespace Flamingo.Models
{
    public class Image
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string LocationSmall { get; set; }
        public string Alt { get; set; }
    }
}