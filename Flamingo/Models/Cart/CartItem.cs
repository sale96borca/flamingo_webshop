using Flamingo.Models.Products;

namespace Flamingo.Models.Cart
{
    public class CartItem
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string ProductImage { get; set; }
        public decimal Total => Quantity * Price;

        public CartItem()
        {
        }
        
        public CartItem(Product product, Image image)
        {
            ProductId = product.Id;
            ProductName = product.Name;
            Quantity = 1;
            Price = product.Price;
            ProductImage = image.Location;
        }
    }
}