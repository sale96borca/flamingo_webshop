using System.Collections.Generic;

namespace Flamingo.Models.Cart
{
    public class CartView
    {
        public List<CartItem> CartItems { get; set; }
        public decimal GrandTotal { get; set; }
        public int GrandQuantity { get; set; }
    }
}