using System.ComponentModel.DataAnnotations;

namespace Flamingo.Models
{
    public class Message
    {
        public int Id { get; set; }
        [Required, Display(Name="Naslov")]
        public string Subject { get; set; }
        [Required, Display(Name="Ime i prezime")]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required, Display(Name="Opis")]
        public string Description { get; set; }
    }
}