using System;
using System.ComponentModel.DataAnnotations;

namespace Flamingo.Models.Brands
{
    public class Brand
    {
        public int Id { get; set; }
        [Required, MinLength(4, ErrorMessage = "Brand name should have minimum of 4 charachters.")]
        public string Name { get; set; }

        [Url]
        public string Url { get; set; }
    }
}