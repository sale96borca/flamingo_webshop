using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Flamingo.Models.Categories;

namespace Flamingo.Models.Brands
{
    public class CategoryBrand
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int BrandId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
        [ForeignKey("BrandId")]
        public virtual Brand Brand { get; set; }
    }
}