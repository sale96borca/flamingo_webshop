using System;
using System.Collections.Generic;
using Flamingo.Models.Categories;

namespace Flamingo.Models.Brands
{
    public class CategoryBrandDisplay
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string Url { get; set; }
        public List<Category> CategoryNames { get; set; }
    }
}