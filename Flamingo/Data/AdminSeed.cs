using System;
using System.Threading.Tasks;
using Flamingo.Models.Customer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Flamingo.Data
{
    public static class AdminSeed
    {
        public static async Task Initialize(
            UserManager<AppCustomer> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            try
            {
                await SeedRole(roleManager);
                await SeedUser(userManager);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private static async Task SeedRole(RoleManager<IdentityRole> roleManager)
        {
            var exist = await roleManager.RoleExistsAsync("Admin");
            if (!exist)
            {
                var role = new IdentityRole();
                role.Name = "Admin";

                var result = await roleManager.CreateAsync(role);
            }
        }

        private static async Task SeedUser(UserManager<AppCustomer> userManager)
        {
            var exist = await userManager.FindByNameAsync("Admin") != null;
            if (!exist)
            {
                var appCustomer = new AppCustomer();

                appCustomer.UserName = "Admin";
                appCustomer.FirstName = "Tatjana";
                appCustomer.LastName = "Stojkovic";
                appCustomer.Email = "admin@admin.com";

                var result = await userManager.CreateAsync(appCustomer, "admin12345");

                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(appCustomer, "Admin");
                }
            }
        }
    }
}