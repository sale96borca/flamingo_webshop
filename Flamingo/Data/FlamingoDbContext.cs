using System;
using Microsoft.EntityFrameworkCore;
using Flamingo.Models;
using Flamingo.Models.Products;
using Flamingo.Models.Brands;
using Flamingo.Models.Categories;
using Flamingo.Models.Checkout;
using Flamingo.Models.Customer;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Flamingo.Data
{
        public class FlamingoDbContext : IdentityDbContext<AppCustomer>
    {
        public FlamingoDbContext(DbContextOptions<FlamingoDbContext> options)
            :base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<CategoryBrand> CategoryBrands { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Message> Messages { get; set; }
    }
}