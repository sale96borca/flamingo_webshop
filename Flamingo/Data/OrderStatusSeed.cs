using System;
using System.Linq;
using Flamingo.Models.Checkout;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Flamingo.Data
{
    public static class OrderStatusSeed
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new FlamingoDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<FlamingoDbContext>>()))
            {
                if (context.OrderStatuses.Any())
                {
                    return;
                }
            
                context.OrderStatuses.AddRange(
                    new OrderStatus
                    {
                        Name = "Canceled"
                    },
                    new OrderStatus
                    {
                        Name = "Onhold"
                    },
                    new OrderStatus
                    {
                        Name = "Pending"
                    },
                    new OrderStatus
                    {
                        Name = "Shipped"
                    }
                );

                context.SaveChanges();    
            }
        }
    }
}