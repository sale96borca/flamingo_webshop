using System.Linq;
using System.Threading.Tasks;
using Flamingo.Data;
using Flamingo.Models.Checkout;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Flamingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class OrdersController : Controller
    {
        private readonly FlamingoDbContext _context;

        public OrdersController(FlamingoDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var orders = await _context.Orders.OrderByDescending(x => x.Id).Include(x => x.OrderStatus).ToListAsync();
            return View(orders);
        }

        public async Task<IActionResult> Detail(int id)
        {
            var order = await _context.Orders
                .Include(x => x.OrderStatus)
                .FirstOrDefaultAsync(x => x.Id == id);

            var orderProduct = await _context.OrderItems
                .Include(x => x.Product)
                .Where(x => x.OrderId == id).ToListAsync();

            ViewBag.OrderItems = orderProduct;
            
            return View(order);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var order = await _context.Orders
                .Include(x => x.OrderStatus)
                .FirstOrDefaultAsync(x => x.Id == id);

            var orderStatuses = await _context.OrderStatuses.ToListAsync();

            ViewBag.OrderStatuses = new SelectList(orderStatuses, "Id", "Name");
            
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Order order)
        {
            var orderStatuses = await _context.OrderStatuses.ToListAsync();
            ViewBag.OrderStatuses = new SelectList(orderStatuses, "Id", "Name");

            if (!ModelState.IsValid) return View(order);

            _context.Orders.Update(order);
            await _context.SaveChangesAsync();

            TempData["Success"] = "Order has been updated!";
            
            return View(order);
        }

        public async Task<IActionResult> Remove(int id)
        {
            var order = await _context.Orders.FirstOrDefaultAsync(x => x.Id == id);

            if (order == null)
            {
                TempData["Error"] = $"Order with the id of {id} does not exist.";
            }
            else
            {
                _context.Orders.Remove(order);
                await _context.SaveChangesAsync();

                TempData["Success"] = "Order has been deleted!";
            }
            
            return RedirectToAction("Index");
        }
    }
}