using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Flamingo.Data;
using Flamingo.Models;
using Flamingo.Models.Brands;
using System.Threading.Tasks;
using System.Collections.Generic;
using Flamingo.Services.Interfaces;
using Flamingo.Services;
using Microsoft.AspNetCore.Authorization;

namespace Flamingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class BrandsController : Controller
    {
        private readonly FlamingoDbContext _context;
        private readonly CategoryBrandService _categoryBrandService;
        public BrandsController(FlamingoDbContext context)
        {
            _context = context;
            _categoryBrandService = new CategoryBrandService(_context);
        }

        public IActionResult Index()
        {
            var brands = _categoryBrandService.GetCollection();

            return View(brands);
        }

        [HttpGet]
        public IActionResult Create() {
            ViewBag.Categories = new SelectList(_context.Categories.OrderBy(x => x.Name), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Brand brand, int[] categoryList)
        {
            ViewBag.Categories = new SelectList(_context.Categories.OrderBy(x => x.Name), "Id", "Name");
            if (categoryList.Length == 0) {
                TempData["Error"] = "Please select category";
                return View(brand);
            }

            if (!ModelState.IsValid) return View(brand);
            
            var exists = await _context.Brands.FirstOrDefaultAsync(x => x.Name == brand.Name);

            if (exists != null) {
                ModelState.AddModelError("", "Brand already exists.");
                return View(brand);
            }

            _context.Add(brand);
            await _context.SaveChangesAsync();

            var id = brand.Id;

            foreach (var categoryId in categoryList) {
                var categoryBrand = new CategoryBrand()
                {
                    CategoryId = categoryId,
                    BrandId = id
                };

                _context.Add(categoryBrand);
                await _context.SaveChangesAsync();
            }

            TempData["Success"] = "Successfully added Brand!";

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id) {
            var brands = await _context.Brands
                .FirstOrDefaultAsync(x => x.Id == id);

            ViewBag.Categories = _categoryBrandService.GetActiveSelectList(id);

            return View(brands);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Brand brand, int[] categoryList)
        {
            ViewBag.Categories = _categoryBrandService.GetActiveSelectList(brand.Id);

            if (categoryList.Length == 0)
            {
                TempData["Error"] = "You must select category!";
                return View(brand);
            }

            if (!ModelState.IsValid) return View(brand);
            
            var currentBrand = await _context.Brands
                .AsNoTracking()
                .Where(x => x.Id != brand.Id)
                .FirstOrDefaultAsync(x => x.Name == brand.Name);

            if (currentBrand != null) 
            {
                ModelState.AddModelError("", $"Category with the name {brand.Name} already exist.");
                return View(brand);
            }

            currentBrand = await _context.Brands
                .Where(x => x.Id == brand.Id)
                .FirstOrDefaultAsync();

            currentBrand.Name = brand.Name;
            currentBrand.Url = brand.Url;

            var areRemoved = await _categoryBrandService.RemoveCollection(brand.Id);

            foreach (var categoryId in categoryList) 
            {
                var categoryBrand = new CategoryBrand()
                {
                    CategoryId = categoryId,
                    BrandId = brand.Id
                };

                _context.Add(categoryBrand);
            }

            _context.Update(currentBrand);
            await _context.SaveChangesAsync();

            TempData["Success"] = "Brand has been updated!";

            ViewBag.Categories = _categoryBrandService.GetActiveSelectList(currentBrand.Id);

            return View(currentBrand);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var brand = await _context.Brands
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            if (brand == null)
            {
                TempData["Error"] = "Brand does not exist.";
                return RedirectToAction("Index");
            }

            _context.Remove(brand);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}