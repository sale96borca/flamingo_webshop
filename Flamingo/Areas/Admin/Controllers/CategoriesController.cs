using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Flamingo.Data;
using Flamingo.Models;
using Flamingo.Models.Categories;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Flamingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class CategoriesController : Controller
    {
        private readonly FlamingoDbContext context;

        public CategoriesController(FlamingoDbContext _context)
        {
            context = _context;
        }
        public IActionResult Index(string search = "", int order = -1)
        {
            var categories = from c in context.Categories
                            select c;

            if (!String.IsNullOrEmpty(search)) {
                categories = categories.Where(x => x.Name.Contains(search));
            }

            if (order != -1) {
                if (order == 1) {
                    categories = categories.OrderBy(x => x.Name);
                } else if(order == 2) {
                    categories = categories.OrderByDescending(x => x.Name);
                }
            }
            
            return View(categories);
        }

        [HttpGet]
        public IActionResult Create() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Category category)
        {
            if (ModelState.IsValid) {
                var exists = await context.Categories
                    .FirstOrDefaultAsync(x => x.Name == category.Name);

                if (exists != null) {
                    ModelState.AddModelError("", $"Category with the name {category.Name} allredy exists.");
                    return View(category);
                }

                context.Add(category);
                await context.SaveChangesAsync();

                TempData["Success"] = "Category successfully added!";

                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var category = await context.Categories.FindAsync(id);
            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Category category)
        {
            if (ModelState.IsValid) {
                var name = await context.Categories
                    .Where(x => x.Id != category.Id)
                    .FirstOrDefaultAsync(x => x.Name == category.Name);

                if (name != null) {
                    ModelState.AddModelError("", "Nothing is changed.");
                    return View(category);
                }

                context.Update(category);
                await context.SaveChangesAsync();

                TempData["Success"] = "Category has been edited!";

                return RedirectToAction("Edit", new {id = category.Id});
            }

            return View(category);
        }

        public async Task<IActionResult> Delete(int id)
        {
            Category category = await context.Categories.FindAsync(id);
            
            if (category == null) {
                TempData["Error"] = "Category does not exist.";
            } else {
                context.Categories.Remove(category);
                await context.SaveChangesAsync();

                TempData["Success"] = "Category has been deleted!";
            }

            return RedirectToAction("Index");
        }
    }
}