using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Flamingo.Data;
using Flamingo.Models;
using Flamingo.Models.Products;
using Flamingo.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Flamingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class ProductImagesController : Controller
    {
        private readonly FlamingoDbContext context;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly ProductImageService imageService;
        public ProductImagesController(
            FlamingoDbContext _context,
            IWebHostEnvironment _webHostEnvironment)
        {
            context = _context;
            webHostEnvironment = _webHostEnvironment;

            imageService = new ProductImageService(context);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(int id, IFormFile productImage)
        {
            string imageName = String.Empty;
            if (productImage == null) {
                TempData["Error"] = "You need to provide image for the upload.";

                return RedirectToAction("Edit", "Products", new { id });
            } else {
                string uploadDir = Path.Combine(webHostEnvironment.WebRootPath, "images/product");
                imageName = Guid.NewGuid().ToString() + "_" + productImage.FileName;
                string filePath = Path.Combine(uploadDir, imageName);

                FileStream fs = new FileStream(filePath, FileMode.Create);
                await productImage.CopyToAsync(fs);
                fs.Close();
            } 

            var product = await context.Products
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            if (product == null) {
                TempData["Error"] = "Product does not exists.";
                return RedirectToAction("Index", "Products");
            }

            var image = new Image
            {
                Location = imageName,
                LocationSmall = imageName,
                Alt = product.Name
            };

            context.Add(image);
            await context.SaveChangesAsync();

            var productImageModel = new ProductImage
            {
                ProductId = product.Id,
                ImageId = image.Id
            };

            context.Add(productImageModel);
            await context.SaveChangesAsync();

            TempData["Success"] = "Image is uploaded successfuly!";

            return RedirectToAction("Edit", "Products", new { id });
        }
    }
}