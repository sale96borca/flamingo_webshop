using System.Linq;
using System.Threading.Tasks;
using Flamingo.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Flamingo.Areas.Admin.Controllers
{
    [Area("Admin"), Authorize(Roles = "Admin")]
    public class MessagesController : Controller
    {
        private readonly FlamingoDbContext _context;

        public MessagesController(FlamingoDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var messages = await _context.Messages.OrderByDescending(x => x.Id).ToListAsync();
            return View(messages);
        }

        public async Task<IActionResult> Detail(int id)
        {
            var message = await _context.Messages.FirstOrDefaultAsync(x => x.Id == id);

            if (message == null)
            {
                TempData["Error"] = "Poruka koju ste trazili ne postoji.";
                return RedirectToAction("Index");
            }

            return View(message);
        }
        
        public async Task<IActionResult> Remove(int id)
        {
            var message = await _context.Messages.FirstOrDefaultAsync(x => x.Id == id);

            if (message == null)
            {
                TempData["Error"] = "The message you tried to delete does not exist.";
                return RedirectToAction("Index");
            }
            
            _context.Messages.Remove(message);
            await _context.SaveChangesAsync();

            TempData["Success"] = "You have deleted a message.";
            
            return RedirectToAction("Index");
        }
    }
}