using System.Threading.Tasks;
using Flamingo.Models.Customer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Flamingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class CustomersController : Controller
    {
        private readonly UserManager<AppCustomer> _userManager;

        public CustomersController(UserManager<AppCustomer> userManager)
        {
            _userManager = userManager;
        }
        
        public IActionResult Index()
        {
            return View(_userManager.Users);
        }

        public async Task<IActionResult> Remove(string name)
        {
            var customer = await _userManager.FindByNameAsync(name);

            if (customer == null)
            {
                TempData["Error"] = $"User with the name {name} does not exist.";
                return RedirectToAction("Index");
            }

            await _userManager.DeleteAsync(customer);
            TempData["Success"] = $"User {name} is successfully deleted.";

            return RedirectToAction("Index");
        }
    }
}