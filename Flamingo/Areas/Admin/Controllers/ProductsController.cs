using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Flamingo.Data;
using Flamingo.Models;
using Flamingo.Models.Products;
using System.Threading.Tasks;
using System.Collections.Generic;
using Flamingo.Services.Interfaces;
using Flamingo.Services;
using Flamingo.Models.Categories;
using Flamingo.Models.Brands;
using Microsoft.AspNetCore.Authorization;

namespace Flamingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class ProductsController : Controller
    {
        private readonly FlamingoDbContext context;
        private readonly ProductImageService imageService;

        private List<Category> categories { get; set; }
        private List<Brand> brands { get; set; }

        public ProductsController(FlamingoDbContext _context)
        {
            context = _context;

            imageService = new ProductImageService(context);

            categories = context.Categories
                .OrderBy(x => x.Id)
                .ToList();

            brands = context.Brands
                .OrderBy(x => x.Id)
                .ToList();
        }

        public async Task<IActionResult> Index()
        {
            var products = await context.Products
                .Include(x => x.Category)
                .Include(x => x.Brand)
                .ToListAsync();

            return View(products);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Categories = new SelectList(categories, "Id", "Name");
            ViewBag.Brands = new SelectList(brands, "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Product product)
        {
            ViewBag.Categories = new SelectList(categories, "Id", "Name");
            ViewBag.Brands = new SelectList(brands, "Id", "Name");

            if (ModelState.IsValid) {
                var barcode = await context.Products
                    .Where(x => x.Barcode == product.Barcode)
                    .FirstOrDefaultAsync();

                if (barcode != null) {
                    ModelState.AddModelError("", $"Product with barcode: {product.Barcode} already exist.");
                    return View(product);
                }

                context.Add(product);
                await context.SaveChangesAsync();

                TempData["Success"] = "Product is successfily added!";
                return RedirectToAction("Index");
            }

            return View(product);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            ViewBag.Categories = new SelectList(categories, "Id", "Name");
            ViewBag.Brands = new SelectList(brands, "Id", "Name");

            ViewBag.Images = await imageService.GetCollectionById(id);

            var product = await context.Products
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Product product)
        {
            ViewBag.Categories = new SelectList(categories, "Id", "Name");
            ViewBag.Brands = new SelectList(brands, "Id", "Name");
            ViewBag.Images = await imageService.GetCollectionById(product.Id);

            if (ModelState.IsValid) {
                context.Update(product);
                await context.SaveChangesAsync();

                TempData["Success"] = "Product has been edited!";

                return View(product);
            }

            TempData["Error"] = "Something went wrong editing the product";
            return View(product);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var product = await context.Products
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            if (product == null) {
                TempData["Error"] = "Product does not exist.";
                return RedirectToAction("Index");
            }

            context.Remove(product);
            await context.SaveChangesAsync();

            TempData["Success"] = "Product has been deleted!";
            return RedirectToAction("Index");
        }
    }
}