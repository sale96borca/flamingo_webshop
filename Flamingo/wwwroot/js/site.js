﻿$(document).ready(function() {
    if ($('.notification').length) {
        setTimeout(() => {
            $('.notification').hide();
        }, 2500);
    }
    
    const deleteButton = $(".delete-btn");
    if (deleteButton.length) {
        deleteButton.click(function(e) {
            let deleteConfirm = confirm("Da li ste sigurni da zelite da obrisete vasu korpu?");
            
            if (!deleteConfirm) {
                e.preventDefault();
            }
        });
    }
});
