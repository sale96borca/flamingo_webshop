$(document).ready(function() {
	if ($('.deleteButton').length) {
		$('.deleteButton').click(function(e) {
			let del = confirm("Are you sure?");
			if (!del) {
				e.preventDefault();
			}
		});
	}
});