using System.Linq;
using System.Threading.Tasks;
using Flamingo.Data;
using Flamingo.Models.Products;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Flamingo.Controllers
{
    public class ProductsController : Controller
    {
        private readonly FlamingoDbContext _context;

        public ProductsController(FlamingoDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(
            int category = 0,
            int priceLess = -1,
            int priceGreat = -1,
            int nameSort = 0)
        {
            var products = await _context.Products
                .Include(x => x.Category)
                .Include(x => x.Brand)
                .OrderByDescending(x => x.Id)
                .Select(
                    x => new ProductFrontView {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Category = x.Category,
                        Brand = x.Brand,
                        Tumbnail = _context.ProductImages
                            .Include(p => p.Image)
                            .FirstOrDefault(p => p.ProductId == x.Id).Image
                    }
                )
                .ToListAsync();
            
            if (category != 0)
            {
                products = products.Where(x => x.Category.Id == category).ToList();
            }

            if (priceLess != -1)
            {
                products = products.Where(x => x.Price <= priceLess).ToList();
            }

            if (priceGreat != -1)
            {
                products = products.Where(x => x.Price >= priceGreat).ToList();
            }

            if (nameSort != 0)
            {
                products = nameSort == 1
                    ? products.OrderBy(x => x.Name).ToList()
                    : products.OrderByDescending(x => x.Name).ToList();
            }

            return View(products);
        }

        public async Task<IActionResult> Detail(int id)
        {
            var product = await _context.Products
                .Where(x => x.Id == id)
                .Include(x => x.Category)
                .Include(x => x.Brand)
                .OrderByDescending(x => x.Id)
                .Select(
                    x => new ProductFrontDetailView {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Category = x.Category,
                        Brand = x.Brand,
                        Images = _context.ProductImages
                            .Include(p => p.Image)
                            .Where(p => p.ProductId == x.Id)
                            .Select(y => y.Image)
                            .ToList()
                    }
                )
                .FirstOrDefaultAsync();

            return View(product);
        }
    }
}