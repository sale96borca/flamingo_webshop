﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Flamingo.Models;
using Flamingo.Data;
using Microsoft.EntityFrameworkCore;
using Flamingo.Models.Products;

namespace Flamingo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly FlamingoDbContext _context;

        public HomeController(
            ILogger<HomeController> logger,
            FlamingoDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var products = await _context.Products
                .Include(x => x.Category)
                .Include(x => x.Brand)
                .OrderByDescending(x => x.Id)
                .Select(
                    x => new ProductFrontView {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Category = x.Category,
                        Brand = x.Brand,
                        Tumbnail = _context.ProductImages
                            .Include(p => p.Image)
                            .FirstOrDefault(p => p.ProductId == x.Id).Image
                    }
                )
                .Take(4)
                .ToListAsync();

            ViewBag.Featured = await _context.Products
                .Where(x => x.IsFeatured == true)
                .Include(x => x.Category)
                .Include(x => x.Brand)
                .OrderByDescending(x => x.Id)
                .Select(
                    x => new ProductFrontView {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description,
                        Price = x.Price,
                        Quantity = x.Quantity,
                        Category = x.Category,
                        Brand = x.Brand,
                        Tumbnail = _context.ProductImages
                            .Include(p => p.Image)
                            .FirstOrDefault(p => p.ProductId == x.Id).Image
                    }
                )
                .ToListAsync();

            return View(products);
        }

        public IActionResult Contact() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Contact(Message message)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Molimo Vas da unesete sve podatke.");
                return View(message);
            }

            _context.Messages.Add(message);
            await _context.SaveChangesAsync();

            TempData["Success"] = "Uspesno ste poslali poruku.";
            
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
