using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flamingo.Data;
using Flamingo.Models;
using Flamingo.Models.Cart;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Flamingo.Controllers
{
    public class CartController : Controller
    {
        private readonly FlamingoDbContext _context;

        public CartController(FlamingoDbContext context)
        {
            _context = context;
        }
        
        public IActionResult Index()
        {
            var cart = HttpContext.Session.GetJson<List<CartItem>>("Cart") ?? new List<CartItem>();

            var cartView = new CartView
            {
                CartItems = cart,
                GrandTotal = cart.Sum(x => x.Price * x.Quantity),
                GrandQuantity = cart.Sum(new Func<CartItem, int>(x => x.Quantity))
            };

            return View(cartView);
        }

        [HttpPost]
        public async Task<IActionResult> Add(int id)
        {
            var product = await _context.Products.FindAsync(id);
            var productImage = await _context.ProductImages
                .Include(x => x.Image)
                .FirstOrDefaultAsync(x => x.ProductId == id);

            var image = new Image();
            if (productImage == null)
            {
                image.Location = "noimage.png";
            }
            else
            {
                image = productImage.Image;
            }
            
            var cart = HttpContext.Session.GetJson<List<CartItem>>("Cart") ?? new List<CartItem>();
            var cartItem = cart.Where(x => x.ProductId == id).FirstOrDefault();

            if (cartItem == null)
            {
                cart.Add(new CartItem(product, image));
            }
            else
            {
                cartItem.Quantity++;
            }
            
            HttpContext.Session.SetJson("Cart", cart);

            return Ok();
        }

        public IActionResult Remove(int id)
        {
            var cart = HttpContext.Session.GetJson<List<CartItem>>("Cart");
            cart.RemoveAll(x => x.ProductId == id);

            if (cart.Count == 0)
            {
                HttpContext.Session.Remove("Cart");
            }
            else
            {
                HttpContext.Session.SetJson("Cart", cart);
            }

            return RedirectToAction("Index");
        }

        public IActionResult Decrement(int id)
        {
            var cart = HttpContext.Session.GetJson<List<CartItem>>("Cart");
            var cartItem = cart.Where(x => x.ProductId == id).FirstOrDefault();

            --cartItem.Quantity;

            if (cartItem.Quantity == 0)
            {
                cart.RemoveAll(x => x.ProductId == id);
            }

            if (cart.Count == 0)
            {
                HttpContext.Session.Remove("Cart");
            }
            else
            {
                HttpContext.Session.SetJson("Cart", cart);
            }

            return RedirectToAction("Index");
        }

        public IActionResult Increment(int id)
        {
            var cart = HttpContext.Session.GetJson<List<CartItem>>("Cart");
            var cartItem = cart.Where(x => x.ProductId == id).FirstOrDefault();

            var product = _context.Products.Find(id);

            if ((cartItem.Quantity + 1) > product.Quantity)
            {
                TempData["Error"] = "Trenutno nemamo na stanju taj broj proizvoda.";
                return RedirectToAction("Index");
            }
            else
            {
                ++cartItem.Quantity;
            }

            if (cart.Count == 0)
            {
                HttpContext.Session.Remove("Cart");
            }
            else
            {
                HttpContext.Session.SetJson("Cart", cart);
            }

            return RedirectToAction("Index");
        }

        public IActionResult RemoveAll()
        {
            HttpContext.Session.Remove("Cart");
            return RedirectToAction("Index");
        }

        public IActionResult SubmitCheckout()
        {
            
            return RedirectToAction("Index", "Checkout");
        }
    }
}