using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flamingo.Data;
using Flamingo.Models.Cart;
using Flamingo.Models.Checkout;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Flamingo.Controllers
{
    public class CheckoutController : Controller
    {
        private readonly FlamingoDbContext _context;

        public CheckoutController(FlamingoDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var cartSession = HttpContext.Session.GetJson<List<CartItem>>("Cart");

            if (cartSession == null)
            {
                return RedirectToAction("Index", "Cart");
            }
            
            var cartView = new CartView
            {
                CartItems = cartSession,
                GrandTotal = cartSession.Sum(x => x.Price * x.Quantity),
                GrandQuantity = cartSession.Sum(new Func<CartItem, int>(x => x.Quantity))
            };

            ViewBag.CurrentSessionItems = cartView;
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(CheckoutView checkout)
        {
            var cartSession = HttpContext.Session.GetJson<List<CartItem>>("Cart");

            if (cartSession == null)
            {
                return RedirectToAction("Index", "Cart");
            }
            
            var cartView = new CartView
            {
                CartItems = cartSession,
                GrandTotal = cartSession.Sum(x => x.Price * x.Quantity),
                GrandQuantity = cartSession.Sum(new Func<CartItem, int>(x => x.Quantity))
            };

            ViewBag.CurrentSessionItems = cartView;

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Something went wrong.");
                return View(checkout);
            }

            foreach (var cartItem in cartView.CartItems)
            {
                var product = _context.Products.Find(cartItem.ProductId);
                product.Quantity -= cartItem.Quantity;

                _context.Products.Update(product);
                await _context.SaveChangesAsync();
            }
            
            var order = new Order(cartView.CartItems);

            order.FirstName = checkout.FirstName;
            order.LastName = checkout.LastName;
            order.City = checkout.City;
            order.Email = checkout.Email;
            order.Price = cartView.GrandTotal;
            order.Zipcode = checkout.Zipcode;
            order.OrderStatusId = 2;
            order.PhoneNumber = checkout.PhoneNumber;
            order.StreetAddress = checkout.StreetAddress;

            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            var orderItems = order.SetItems();
            
            await _context.OrderItems.AddRangeAsync(orderItems);
            await _context.SaveChangesAsync();
            
            HttpContext.Session.SetString("Checkout", "Finished");
            
            return RedirectToAction("Success");
        }

        public IActionResult Success()
        {
            if (HttpContext.Session.GetString("Checkout") != "Finished")
            {
                return RedirectToAction("Index");
            }

            HttpContext.Session.Remove("Cart");
            HttpContext.Session.Remove("Checkout");
            return View();
        }
        
    }
}