using System.Threading.Tasks;
using Flamingo.Models.Customer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Flamingo.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<AppCustomer> _userManager;
        private readonly SignInManager<AppCustomer> _signInManager;

        public AccountController(
            UserManager<AppCustomer> userManager,
            SignInManager<AppCustomer> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        
        [AllowAnonymous]
        public IActionResult Register() => View();

        [AllowAnonymous]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(User user)
        {
            if (ModelState.IsValid)
            {
                var appCustomer = new AppCustomer
                {
                    UserName = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                    StreetAddress = user.StreetAddress,
                    Zipcode = user.Zipcode,
                    City = user.City
                };

                IdentityResult result = await _userManager.CreateAsync(appCustomer, user.Password);

                if (result.Succeeded)
                {
                    return RedirectToAction("Login");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }
            }

            return View(user);
        }

        [AllowAnonymous]
        public IActionResult Login(string returnUrl)
        {
            Login login = new Login
            {
                ReturnUrl = returnUrl
            };
            
            return View(login);
        }
        
        [AllowAnonymous]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(Login login)
        {
            if (ModelState.IsValid)
            {
                var appCustomer = await _userManager.FindByEmailAsync(login.Email);

                if (appCustomer != null)
                {
                    Microsoft.AspNetCore.Identity.SignInResult result =
                        await _signInManager.PasswordSignInAsync(appCustomer, login.Password, false, false);

                    if (result.Succeeded)
                    {
                        return Redirect(login.ReturnUrl ?? "/");
                    }
                }
                
                ModelState.AddModelError("", "Podaci koje ste uneli nisu validni.");
            }

            return View(login);
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return Redirect("/");
        }

        public async Task<IActionResult> Edit()
        {
            var appCustomer = await _userManager.FindByNameAsync(User.Identity.Name);

            var user = new CustomerEdit(appCustomer);

            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CustomerEdit user)
        {
            if (ModelState.IsValid)
            {
                var appCustomer = await _userManager.FindByNameAsync(User.Identity.Name);

                appCustomer.UserName = user.UserName;
                appCustomer.FirstName = user.FirstName;
                appCustomer.LastName = user.LastName;
                appCustomer.Email = user.Email;
                appCustomer.PhoneNumber = user.PhoneNumber;
                appCustomer.StreetAddress = user.StreetAddress;
                appCustomer.Zipcode = user.Zipcode;
                appCustomer.City = user.City;
                
                var result = await _userManager.UpdateAsync(appCustomer);

                if (result.Succeeded)
                {
                    TempData["Success"] = "Vase informacije su uspesno izmenjene!";
                    return View(user);
                }
                
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            return View(user);
        }
    }
}